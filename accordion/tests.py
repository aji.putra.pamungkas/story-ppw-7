from django.test import TestCase, Client
from django.urls import resolve
from .views import accordions

# Create your tests here.
class accordionTest(TestCase):
    def test_accordion_urls_is_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)
    def test_accordion_templates_is_exist(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'accordion/accordion.html')
    def test_accordion_views_is_exist_using_func(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, accordions)
        
    
