from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.

def accordions(request):
    response={
        'name' : 'Accordion',}
    return render(request, 'accordion/accordion.html',response)