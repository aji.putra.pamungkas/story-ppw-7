from django import forms
from . import models

class input_data_kegiatan_1(forms.ModelForm):
    class Meta:
        model = models.Kegiatan_1
        fields = ['nama']
    error_messages = {
		'required' : 'Please Type'
	}
    input_attrs1 = {
		'type' : 'text',
		'placeholder' : 'Nama Kamu'
	}
    nama = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs1))

class input_data_kegiatan_2(forms.ModelForm):
    class Meta:
        model = models.Kegiatan_2
        fields = ['nama']
    error_messages = {
		'required' : 'Please Type'
	}
    input_attrs1 = {
		'type' : 'text',
		'placeholder' : 'Nama Kamu'
	}
    nama = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs1))

class input_data_kegiatan_3(forms.ModelForm):
    class Meta:
        model = models.Kegiatan_3
        fields = ['nama']
    error_messages = {
		'required' : 'Please Type'
	}
    input_attrs1 = {
		'type' : 'text',
		'placeholder' : 'Nama Kamu'
	}
    nama = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs1))

