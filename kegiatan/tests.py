from django.test import TestCase, Client
from django.urls import resolve
from .views import kegiatan, join_1, join_2, join_3

# Create your tests here.

class kegiatanTest(TestCase):
    def test_kegiatan_urls_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_templates(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')

    def test_kegiatan_using_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, kegiatan)

    def test_join_1_using_func(self):
        found = resolve('/kegiatan/join-1/')
        self.assertEqual(found.func, join_1)

    def test_join_2_using_func(self):
        found = resolve('/kegiatan/join-2/')
        self.assertEqual(found.func, join_2)

    def test_join_3_using_func(self):
        found = resolve('/kegiatan/join-3/')
        self.assertEqual(found.func, join_3)

    

    