from django.urls import path

from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.kegiatan, name = "kegiatan"),
    path('join-1/', views.join_1, name = "join_1"),
    path('join-2/', views.join_2, name = "join_2"),
    path('join-3/', views.join_3, name = "join_3"),
]
