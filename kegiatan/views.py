from django.shortcuts import render
from django.http import HttpResponseRedirect
from . import models, forms

# Create your views here.

def kegiatan(request):
    response={
        'name' : 'Kegiatan',
        'kegiatan1' : models.Kegiatan_1.objects.all(),
        'kegiatan2' : models.Kegiatan_2.objects.all(),
        'kegiatan3' : models.Kegiatan_3.objects.all(),
        'form1' : forms.input_data_kegiatan_1,
        'form2' : forms.input_data_kegiatan_2,
        'form3' : forms.input_data_kegiatan_3,
        }
    return render(request, 'kegiatan/kegiatan.html',response)

def join_1(request):
    if request.method == 'POST':
        form = forms.input_data_kegiatan_1(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            namaBaru = models.Kegiatan_1(nama = nama)
            namaBaru.save()
            return HttpResponseRedirect('/kegiatan')
        else:
            return HttpResponseRedirect('/kegiatan')

def join_2(request):
    if request.method == 'POST':
        form = forms.input_data_kegiatan_2(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            namaBaru = models.Kegiatan_2(nama = nama)
            namaBaru.save()
            return HttpResponseRedirect('/kegiatan')
        else:
            return HttpResponseRedirect('/kegiatan')

def join_3(request):
    if request.method == 'POST':
        form = forms.input_data_kegiatan_3(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            namaBaru = models.Kegiatan_3(nama = nama)
            namaBaru.save()
            return HttpResponseRedirect('/kegiatan')
        else:
            return HttpResponseRedirect('/kegiatan')

