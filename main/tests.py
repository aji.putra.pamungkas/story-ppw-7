from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import home, aboutme, project

"""
@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    Base class for functional test cases with selenium.""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
"""
class mainTest(TestCase):
    def test_main_urls_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_main_templates(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_main_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)


class aboutMeTest(TestCase):
    def test_about_me_urls_is_exist(self):
        response = Client().get('/about-me/')
        self.assertEqual(response.status_code, 200)

    def test_about_me_templates(self):
        response = Client().get('/about-me/')
        self.assertTemplateUsed(response, 'main/aboutme.html')

    def test_about_me_using_func(self):
        found = resolve('/about-me/')
        self.assertEqual(found.func, aboutme)

class projectTest(TestCase):
    def test_project_urls_is_exist(self):
        response = Client().get('/project/')
        self.assertEqual(response.status_code, 200)

    def test_project_templates(self):
        response = Client().get('/project/')
        self.assertTemplateUsed(response, 'main/project.html')

    def test_project_using_func(self):
        found = resolve('/project/')
        self.assertEqual(found.func, project)

    
