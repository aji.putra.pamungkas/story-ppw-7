from django.shortcuts import render


def home(request):
    response = {'name' : 'Story 7 PPW'}
    return render(request, 'main/home.html', response)

def aboutme(request):
    response = {'name' : 'Tentang aku'}
    return render(request, 'main/aboutme.html',response)

def project(request):
    response = {'name' : 'Pengalamanku'}
    return render(request, 'main/project.html',response)

