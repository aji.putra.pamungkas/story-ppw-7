from django.test import TestCase, Client
from django.urls import resolve
from .views import matkul, tambah, delete, deleteall
from .models import Matkul

# Create your tests here.

class matkulTest(TestCase):
    def test_matkul_urls_is_exist(self):
        response = Client().get('/matkul/')
        self.assertEqual(response.status_code, 200)

    def test_matkul_templates(self):
        response = Client().get('/matkul/')
        self.assertTemplateUsed(response, 'matkul/matkul.html')

    def test_matkul_using_func(self):
        found = resolve('/matkul/')
        self.assertEqual(found.func, matkul)


    def test_tambah_matkul_using_func(self):
        found = resolve('/matkul/tambah/')
        self.assertEqual(found.func, tambah)


    def test_delete_matkul_using_func(self):
        found = resolve('/matkul/delete/')
        self.assertEqual(found.func, delete)


    def test_delete_all_matkul_using_func(self):
        found = resolve('/matkul/deleteall/')
        self.assertEqual(found.func, deleteall)

    def test_tambah_delete_delete_all_url(self):
        response=Client().get('/matkul/tambah/')
        self.assertEqual(response.status_code, 302)
        response=Client().get('/matkul/delete/')
        self.assertEqual(response.status_code, 302)
        response=Client().get('/matkul/deleteall/')
        self.assertEqual(response.status_code, 302)

    def test_matkul_models(self):
        new_matkul = Matkul.objects.create(nama_matkul = "a", dosen = "b", sks = "c", deskripsi = "d", semester = "e" )
        new_matkul.save()
        count = Matkul.objects.all().count()
        self.assertEqual(count,1)