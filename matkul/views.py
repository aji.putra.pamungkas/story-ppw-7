from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Matkul
from .forms import input_data, input_delete

def matkul(request):
    matkul = Matkul.objects.all()
    response = {'matkuls' : matkul,
     'name' : 'Matkul',
     'form' : input_data,
     'form2' : input_delete,
     }
    return render(request, 'matkul/matkul.html', response)

def tambah(request):
    if request.method == 'POST':
        form = input_data(request.POST)
        if form.is_valid():
            name = form.cleaned_data['nama_matkul']
            dosen = form.cleaned_data['dosen']
            sks = form.cleaned_data['sks']
            deskripsi = form.cleaned_data['deskripsi']
            semester = form.cleaned_data['semester']
            matkul = Matkul(nama_matkul = name, dosen= dosen, sks=sks, deskripsi=deskripsi, semester=semester)
            matkul.save()
        return HttpResponseRedirect('/matkul/')
    else:
        return HttpResponseRedirect('/matkul/')

def delete(request):
    if request.method == 'POST':
        form = input_delete(request.POST)
        if form.is_valid():
            n = form.cleaned_data['nama_matkul']
            matkul = Matkul.objects.filter(nama_matkul = n)
            matkul.delete()
        return HttpResponseRedirect('/matkul/')
    else:
        return HttpResponseRedirect('/matkul/')
   
def deleteall(request):
    matkul = Matkul.objects.all()
    matkul.delete()
    return HttpResponseRedirect('/matkul/')
