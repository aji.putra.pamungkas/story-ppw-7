$(document).ready(function () {
    $('#accordion').accordion({
        collapsible: true,
        active: false,
        height: 'fill',
        header: 'h3',
        event:"dblclick"
        
    });
    
    $('.btn-down').click(function (e) {
        var self = $(this),
            item = self.parents('div.panel'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
    
    $('.btn-up').click(function (e) {
        var self = $(this),
            item = self.parents('div.panel'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
});
    

